package cz.sda.cz8.first.ui;

import cz.sda.cz8.first.cyclicdep.B;
import cz.sda.cz8.first.entity.Language;
import cz.sda.cz8.first.services.DictionaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@Slf4j
public class CommandLineRunnerUI implements CommandLineRunner {
    @Autowired
    private DictionaryService service;

    @Autowired
    B b;

    @Override
    public void run(String... args) throws Exception {
        b.getA();
        printHelp();
        Scanner scanner = new Scanner(System.in);
        while (processLine(scanner.nextLine())) ;
    }

    private void printHelp() {

        log.info("""
                   ,--.                                                                                                                                                                          ,--.    \s
                 .-,|  |,-.  ,---.               ,--.                        ,--.                                         ,--.                              ,--.          ,--.                 .-,|  |,-. \s
                 _\\ '  ' /_ '   .-' ,---. ,--.--.`--',--,--,  ,---.        ,-|  |,--.,--.,--,--,--.,--,--,--.,--. ,--.  ,-'  '-.,--.--.,--,--.,--,--,  ,---.|  | ,--,--.,-'  '-. ,---. ,--.--. _\\ '  ' /_ \s
                (__      __)`.  `-.| .-. ||  .--',--.|      \\| .-. |      ' .-. ||  ||  ||        ||        | \\  '  /   '-.  .-'|  .--' ,-.  ||      \\(  .-'|  |' ,-.  |'-.  .-'| .-. ||  .--'(__      __)\s
                  / .  . \\  .-'    | '-' '|  |   |  ||  ||  |' '-' ',----.\\ `-' |'  ''  '|  |  |  ||  |  |  |  \\   ',----.|  |  |  |  \\ '-'  ||  ||  |.-'  `)  |\\ '-'  |  |  |  ' '-' '|  |     / .  . \\  \s
                 `-'|  |`-' `-----'|  |-' `--'   `--'`--''--'.`-  / '----' `---'  `----' `--`--`--'`--`--`--'.-'  / '----'`--'  `--'   `--`--'`--''--'`----'`--' `--`--'  `--'   `---' `--'    `-'|  |`-' \s
                    `--'           `--'                      `---'                                           `---'                                                                                `--'    \s           
                """);
        log.info("Usage:");
        log.info("""
                 stop
                 add czech english
                 list
                 translate cz|en word --- cz|en meaning source language
                 i,e translate cz cat will return kocka:cat
                """);

    }

    private boolean processLine(String line) {

        if (line.equalsIgnoreCase("stop")) {
            return false;
        }
        String[] commands = line.split(" ");
        switch (commands[0].toLowerCase()) {
            case "add" -> service.addTranslate(commands[1], commands[2]);
            case "list" -> log.info(service.getAll());
            case "translate" -> {
                String translateForWord = service.getTranslateForWord(commands[2], Language.valueOf(commands[1].toUpperCase()));
                log.info(translateForWord);
            }
            default -> log.info("Unknown command");
        }
        // add czech english
        // list
        // translate cz|en word --- cz|en meaning source language
        //i,e translate cz cat will return kocka:cat
        return true;
    }
}
