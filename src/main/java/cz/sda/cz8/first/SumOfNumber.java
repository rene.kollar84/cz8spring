package cz.sda.cz8.first;

import java.util.stream.LongStream;

public class SumOfNumber {
    public static void main(String[] args) {
        long a = 1;
        long b = 1050000000L;
        long sum = count(a, b);
        System.out.println(sum);
    }

    private static long count(long from, long to) {
        long start = System.currentTimeMillis();

        //1+2+3+4..+100

        //1+2+3+4..+99+98+97+100 +
        //100+99+98+97...+4+3+2+1
        // vys=vys/2

        //101+101+101... nkrat 100
        //101*100 /2

        //code here
        long sum = sum(from, to);
//        for(long i=from;i<=to;i++){
//            sum +=i;
//        }
        long end = System.currentTimeMillis();
        System.out.println("Duration: " + (end - start));
        return sum;
    }


    static long sum(long a, long b) {
        return ((b - a + 1) * (a + b)) / 2;
    }
}
