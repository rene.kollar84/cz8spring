package cz.sda.cz8.first.cyclicdep;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class B {


    private final ApplicationContext context;
    A a;
    public B(ApplicationContext context) {
        this.context = context;
    }

    public A getA() {
        a = (A) context.getBean("a");
        return a;
    }
}
