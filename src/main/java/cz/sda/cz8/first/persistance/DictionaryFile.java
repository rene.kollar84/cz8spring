package cz.sda.cz8.first.persistance;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.sda.cz8.first.TranslatorException;
import cz.sda.cz8.first.dummy_persist.Dictionary;
import cz.sda.cz8.first.entity.Language;
import cz.sda.cz8.first.entity.Translate;
import cz.sda.cz8.first.entity.Word;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import java.util.ArrayList;
import java.util.List;

@Component
@Primary
public class DictionaryFile implements DictionaryPersistentInterface {

    ObjectMapper mapper = new ObjectMapper();


    File file;

    private Dictionary proxy;

    public DictionaryFile(File f) throws IOException {
        this.file = f;
       load();
    }

    @Override
    public List<Translate> getAllTranslates() {
        return proxy.getAllTranslates();
    }

    @Override
    public void addTranslate(Translate translate) {
        proxy.addTranslate(translate);
        try {
            save();
        } catch (IOException e) {
            throw new TranslatorException(e.getMessage());
        }
    }

    @Override
    public Word getTranslateForWord(String word, Language language) {
        return proxy.getTranslateForWord(word, language);
    }

    public void save() throws IOException {
        List<Translate> allTranslates = getAllTranslates();
        String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(allTranslates);
        StandardOpenOption option = file.exists()? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE;
        Files.write(file.toPath(), s.getBytes(), option);
    }

    public void load() throws IOException {
        Translate[] asList = file.exists() ? mapper.readValue(file, Translate[].class) : new Translate[0];
        proxy = new Dictionary(new ArrayList<>(List.of(asList)));

    }
}
