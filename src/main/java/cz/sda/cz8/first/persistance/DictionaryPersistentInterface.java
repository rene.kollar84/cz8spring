package cz.sda.cz8.first.persistance;

import cz.sda.cz8.first.entity.Language;
import cz.sda.cz8.first.entity.Translate;
import cz.sda.cz8.first.entity.Word;

import java.util.List;

public interface DictionaryPersistentInterface {

    List<Translate> getAllTranslates();
    void addTranslate(Translate translate) ;
    Word getTranslateForWord(String word, Language language);
}
