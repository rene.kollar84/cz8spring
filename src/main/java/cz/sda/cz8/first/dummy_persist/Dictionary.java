package cz.sda.cz8.first.dummy_persist;

import cz.sda.cz8.first.TranslatorException;
import cz.sda.cz8.first.entity.Language;
import cz.sda.cz8.first.entity.Translate;
import cz.sda.cz8.first.entity.Word;
import cz.sda.cz8.first.persistance.DictionaryPersistentInterface;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component("dummyDictionary")
public class Dictionary implements DictionaryPersistentInterface {

    private List<Translate> db;

    public Dictionary(List<Translate> db) {
        this.db = db;
    }

    public List<Translate> getAllTranslates() {
        return db;
    }

    public void addTranslate(Translate translate) {
        db.add(translate);
    }

    public Word getTranslateForWord(String word, Language language) {

        Optional<Translate> optTranslat = db.stream().filter(translate -> translate.getDest().getValue().equals(word) || translate.getSource().getValue().equals(word))
                .findFirst();
        if(optTranslat.isPresent()) {
            Translate translate1 = optTranslat.get();
            return translate1.getSource().getLanguage() == language ? translate1.getSource() : translate1.getDest();
        }else{
            throw new TranslatorException("Not present");
        }
    }
}
