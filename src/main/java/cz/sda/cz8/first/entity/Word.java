package cz.sda.cz8.first.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Word {
    String value;
    Language language;
}
