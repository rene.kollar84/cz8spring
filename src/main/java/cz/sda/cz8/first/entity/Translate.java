package cz.sda.cz8.first.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Translate {
    Word source;//kocka,cz
    Word dest;//cat,en
}
