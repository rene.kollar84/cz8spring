package cz.sda.cz8.first.services;

import cz.sda.cz8.first.entity.Language;
import cz.sda.cz8.first.entity.Translate;
import cz.sda.cz8.first.entity.Word;

import cz.sda.cz8.first.persistance.DictionaryPersistentInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DictionaryService {
    @Autowired
   // @Qualifier("dummyDictionary")
    private DictionaryPersistentInterface persistence;

    public String getAll() {
        Optional<String> all = persistence.getAllTranslates().stream()
                .map(translate -> translate.getSource() + "-" + translate.getDest())
                .reduce((s, s2) -> s + "\n"+s2);
        if (all.isPresent()){
            return all.get();
        } else {
            return "Empty dictionary";
        }
    }

    public void addTranslate(String czechWord, String englishWord) {
        persistence.addTranslate(new Translate(new Word(czechWord, Language.CZ), new Word(englishWord, Language.EN)));
    }

    public String getTranslateForWord(String word, Language language) {
        Word translateForWord = persistence.getTranslateForWord(word, language);
        return word + ":" + translateForWord.getValue();
    }
}
