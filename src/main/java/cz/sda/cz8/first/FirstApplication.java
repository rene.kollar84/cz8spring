package cz.sda.cz8.first;

import cz.sda.cz8.first.entity.Translate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class FirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirstApplication.class, args);
    }

    @Bean
    public List<Translate> getMemoryDb() {
        return new ArrayList<>();
    }

    @Bean
    public File getFile(@Value("${dummy.translator.file}") String path){
        return new File(path);
    }
}
