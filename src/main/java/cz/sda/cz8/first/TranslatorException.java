package cz.sda.cz8.first;

public class TranslatorException extends RuntimeException {
    public TranslatorException(String message) {
        super(message);
    }
}
